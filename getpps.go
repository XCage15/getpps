package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gosuri/uilive"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type iface struct {
	name         string
	r_bytes      int
	r_packets    int
	r_errs       int
	r_drop       int
	r_fifo       int
	r_frame      int
	r_compressed int
	r_multicast  int
	t_bytes      int
	t_packets    int
	t_errs       int
	t_drop       int
	t_fifo       int
	t_colls      int
	t_carrier    int
	t_compressed int
}

func str2int(str string) int {
	i, _ := strconv.Atoi(str)
	return i
}

func parse(line string) iface {
	each_fe := strings.Fields(line)
	tmp1 := iface{
		name:         each_fe[0][:len(each_fe[0])-1],
		r_bytes:      str2int(each_fe[1]),
		r_packets:    str2int(each_fe[2]),
		r_errs:       str2int(each_fe[3]),
		r_drop:       str2int(each_fe[4]),
		r_fifo:       str2int(each_fe[5]),
		r_frame:      str2int(each_fe[6]),
		r_compressed: str2int(each_fe[7]),
		r_multicast:  str2int(each_fe[8]),
		t_bytes:      str2int(each_fe[9]),
		t_packets:    str2int(each_fe[10]),
		t_errs:       str2int(each_fe[11]),
		t_drop:       str2int(each_fe[12]),
		t_fifo:       str2int(each_fe[13]),
		t_colls:      str2int(each_fe[14]),
		t_carrier:    str2int(each_fe[15]),
		t_compressed: str2int(each_fe[16]),
	}
	return tmp1
}

func readNetdata() []string {
	var text []string
	file, err := os.Open("/proc/net/dev")
	check(err)
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		text = append(text, scanner.Text())
	}
	file.Close()
	return text
}

func compareData(tmp1 map[string]iface, tmp2 map[string]iface) map[string]iface {
	var tmp = make(map[string]iface)
	for key, _ := range tmp1 {
		tmp[key] = iface{
			name:         key,
			r_bytes:      tmp1[key].r_bytes - tmp2[key].r_bytes,
			r_packets:    tmp1[key].r_packets - tmp2[key].r_packets,
			r_errs:       tmp1[key].r_errs - tmp2[key].r_errs,
			r_drop:       tmp1[key].r_drop - tmp2[key].r_drop,
			r_fifo:       tmp1[key].r_fifo - tmp2[key].r_fifo,
			r_frame:      tmp1[key].r_frame - tmp2[key].r_frame,
			r_compressed: tmp1[key].r_compressed - tmp2[key].r_compressed,
			r_multicast:  tmp1[key].r_multicast - tmp2[key].r_multicast,
			t_bytes:      tmp1[key].t_bytes - tmp2[key].t_bytes,
			t_packets:    tmp1[key].t_packets - tmp2[key].t_packets,
			t_errs:       tmp1[key].t_errs - tmp2[key].t_errs,
			t_drop:       tmp1[key].t_drop - tmp2[key].t_drop,
			t_fifo:       tmp1[key].t_fifo - tmp2[key].t_fifo,
			t_colls:      tmp1[key].t_colls - tmp2[key].t_colls,
			t_carrier:    tmp1[key].t_carrier - tmp2[key].t_carrier,
			t_compressed: tmp1[key].t_compressed - tmp2[key].t_compressed,
		}
	}
	return tmp
}

func bytesUnit(data int) string {
	unit := " B/s"
	data1 := float32(data)
	if data > 1073741824 {
		unit = "GB/s"
		data1 = data1 / 1073741824
	} else if data > 1048576 {
		unit = "MB/s"
		data1 = data1 / 1048576
	} else if data > 1024 {
		unit = "KB/s"
		data1 = data1 / 1024
	}
	return fmt.Sprintf("%.2f", data1) + unit
}

func bitUnit(data int) string {
	unit := " b/s"
	data1 := float32(data) * 8
	if data1 > 1073741824 {
		unit = "Gb/s"
		data1 = data1 / 1073741824
	} else if data1 > 1048576 {
		unit = "Mb/s"
		data1 = data1 / 1048576
	} else if data1 > 1024 {
		unit = "Kb/s"
		data1 = data1 / 1024
	}
	return fmt.Sprintf("%.2f", data1) + unit
}

func ppsUnit(data int) string {
	unit := " pps"
	data1 := float32(data)
	if data > 1000000 {
		unit = "Mpps"
		data1 = data1 / 1000000
	} else if data > 1000 {
		unit = "Kpps"
		data1 = data1 / 1000
	}
	return fmt.Sprintf("%.2f", data1) + unit
}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

func main() {
	args := os.Args
	has_valid_args := false
	var inf_list []string
	text := readNetdata()
	if len(args) == 2 {
		ginf_list := strings.Split(args[1], ",")
		var tinf_list []string
		text := readNetdata()
		for _, inf1 := range text {
			if strings.Contains(inf1, ":") {
				inf1_fields := strings.Fields(inf1)
				tinf_list = append(tinf_list, inf1_fields[0][:len(inf1_fields[0])-1])
			}
		}
		for _, inf1 := range ginf_list {
			if contains(tinf_list, inf1) {
				inf_list = append(inf_list, inf1)
			}
		}
		if len(inf_list) > 0 {
			has_valid_args = true
		}
	}
	if !has_valid_args {
		for _, inf1 := range text {
			if strings.Contains(inf1, ":") {
				inf1_fields := strings.Fields(inf1)
				inf_list = append(inf_list, inf1_fields[0][:len(inf1_fields[0])-1])
			}
		}
	}

	initialized := false
	var tmp2 = make(map[string]iface)
	writer := uilive.New()
	writer.Start()
	for {
		n := 0
		for n < 2 {
			text := readNetdata()
			tmp1 := make(map[string]iface)
			for _, line := range text {
				if strings.Contains(line, ":") {
					field := strings.Fields(line)
					ifname := field[0][:len(field[0])-1]
					tmp1[ifname] = parse(line)
				}
			}

			if initialized {
				output := fmt.Sprint("\n")
				output += fmt.Sprintf("%10s|%13s|%13s|%13s|%13s|%8s|%8s|%8s|%8s|\n", "IFname", "Rev bit/s", "Tra bit/s", "Rev PPS", "Tra PPS", "RevErr/s", "TraErr/s", "RevDrp/s", "TraDrp/s")
				output += fmt.Sprintf("%103s\n", strings.Repeat("-", 103))
				diff := compareData(tmp1, tmp2)
				for _, key := range inf_list {
					output += fmt.Sprintf("%10s|%13s|%13s|%13s|%13s|%8d|%8d|%8d|%8d|\n",
						key,
						bitUnit(diff[key].r_bytes), bitUnit(diff[key].t_bytes), ppsUnit(diff[key].r_packets), ppsUnit(diff[key].t_packets), diff[key].r_errs, diff[key].t_errs, diff[key].r_drop, diff[key].t_drop,
					)
				}
				output += fmt.Sprintf("%103s\n", strings.Repeat("=", 103))
				fmt.Fprintf(writer, output)
			}
			time.Sleep(1 * time.Second)
			initialized = true
			tmp2 = tmp1
			n += 1
		}

	}
	writer.Stop()
}
